create table admin
(
    id         int auto_increment comment '管理员ID'
        primary key,
    admin_name varchar(50)  not null comment '管理员账号',
    password   varchar(50)  not null comment '密码',
    name       varchar(50)  not null comment '用户名',
    avatar     varchar(255) null comment '头像地址',
    phone      varchar(50)  null comment '手机号',
    email      varchar(50)  null comment '邮箱地址'
)
    comment '管理员表';

create table category
(
    type_id   int auto_increment comment '类别ID'
        primary key,
    type_name varchar(50) not null comment '类别名称'
)
    comment '帖子分类表';

create table notice
(
    info_id int auto_increment comment '公告ID'
        primary key,
    title   varchar(100)  not null comment '标题',
    content text          not null comment '内容',
    time    datetime      not null comment '发布时间',
    image   varchar(255)  null comment '标题图',
    name    varchar(50)   not null comment '作者姓名',
    state   int default 0 not null comment '轮播图状态'
)
    comment '校园公告';

create table user
(
    user_id      int auto_increment comment '用户ID'
        primary key,
    user_name    varchar(50)  not null comment '用户账号',
    password     varchar(50)  not null comment '密码',
    name         varchar(50)  not null comment '用户名',
    sex          varchar(50)  null comment '性别',
    email        varchar(100) null comment '邮箱',
    phone        varchar(20)  null comment '手机号',
    avatar       varchar(255) null comment '头像',
    introduction text         null comment '个人简介'
)
    comment '用户表';

create table course
(
    course_id   int auto_increment comment '课程ID'
        primary key,
    course_name varchar(100) not null comment '课程名称',
    teacher     varchar(50)  null comment '教师姓名',
    classroom   varchar(50)  null comment '教室',
    courseTime  varchar(255) not null comment '上课时间(上课周次:星期:开始节次:结束节次)',
    user_id     int          not null comment '用户ID',
    constraint course_user_user_id_fk
        foreign key (user_id) references user (user_id)
            on update cascade on delete cascade
)
    comment '用户课程表';

create table note
(
    note_id   int auto_increment comment '笔记ID'
        primary key,
    user_id   int          not null comment '用户ID',
    title     varchar(255) null comment '标题',
    content   text         null comment '内容',
    note_time datetime     not null comment '记录时间',
    constraint note_user_user_id_fk
        foreign key (user_id) references user (user_id)
            on update cascade on delete cascade
)
    comment '学习笔记';

create table post
(
    post_id int auto_increment comment '帖子ID'
        primary key,
    user_id int          not null comment '用户ID',
    title   varchar(255) not null comment '标题',
    content text         not null comment '内容',
    type_id int          not null comment '帖子类型ID',
    time    datetime     not null comment '发帖时间',
    constraint post_category_type_id_fk
        foreign key (type_id) references category (type_id)
            on update cascade on delete cascade,
    constraint post_user_user_id_fk
        foreign key (user_id) references user (user_id)
            on update cascade on delete cascade
)
    comment '帖子表';

create table reply
(
    reply_id      int auto_increment comment '回帖ID'
        primary key,
    user_id       int      not null comment '用户ID',
    post_id       int      not null comment '帖子ID',
    reply_content text     not null comment '回复内容',
    time          datetime not null comment '回复时间',
    constraint reply_post_post_id_fk
        foreign key (post_id) references post (post_id)
            on update cascade on delete cascade,
    constraint reply_user_user_id_fk
        foreign key (user_id) references user (user_id)
            on update cascade on delete cascade
)
    comment '回帖表';


