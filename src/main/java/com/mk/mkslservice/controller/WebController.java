package com.mk.mkslservice.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.mk.mkslservice.model.Admin;
import com.mk.mkslservice.service.AdminService;
import com.mk.mkslservice.utils.Result;
import com.mk.mkslservice.utils.ResultCodeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 后台登录控制器
 *
 * @author Shixue
 * @since 2024-01-19
 */
@RestController
@Api(tags = "后台登录")
@RequestMapping("/web")
public class WebController {
    @Resource
    private AdminService adminService;

    @GetMapping("/")
    @ApiOperation("首页")
    public Result hello() {
        return Result.success("访问成功");
    }

    @PostMapping("/login")
    @ApiOperation("登录")
    public Result login(@RequestBody Admin admin) {
        if (ObjectUtil.isEmpty(admin.getAdminName()) || ObjectUtil.isEmpty(admin.getPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        admin = adminService.login(admin);
        return Result.success(ResultCodeEnum.SUCCESS_LOGIN,admin);
    }

    @PostMapping("/register")
    @ApiOperation("注册管理员账户")
    public Result register(@RequestBody Admin admin) {
        if (StrUtil.isBlank(admin.getAdminName()) || StrUtil.isBlank(admin.getPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        adminService.register(admin);
        return Result.success();
    }

    @PutMapping("/updatePassword")
    @ApiOperation("修改密码")
    public Result updatePassword(@RequestBody Admin admin) {
        if (StrUtil.isBlank(admin.getAdminName()) || StrUtil.isBlank(admin.getPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        adminService.updatePassword(admin);
        return Result.success();
    }
}
