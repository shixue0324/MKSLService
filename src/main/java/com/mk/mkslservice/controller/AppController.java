package com.mk.mkslservice.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.mk.mkslservice.model.User;
import com.mk.mkslservice.service.UserService;
import com.mk.mkslservice.utils.Result;
import com.mk.mkslservice.utils.ResultCodeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * APP登录控制器
 *
 * @author Shixue
 * @since 2024-01-19
 */
@RestController
@Api(tags = "用户登录")
@RequestMapping("/app")
public class AppController {
    @Resource
    private UserService userService;

    @PostMapping("/login")
    @ApiOperation("登录")
    public Result login(@RequestBody User user) {
        if (ObjectUtil.isEmpty(user.getUserName()) || ObjectUtil.isEmpty(user.getPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        user = userService.login(user);
        return Result.success(ResultCodeEnum.SUCCESS_LOGIN,user);
    }

    @PostMapping("/register")
    @ApiOperation("注册账户")
    public Result register(@RequestBody User user) {
        if (StrUtil.isBlank(user.getUserName()) || StrUtil.isBlank(user.getPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        userService.register(user);
        return Result.success();
    }

    @PutMapping("/updatePassword")
    @ApiOperation("修改密码")
    public Result updatePassword(@RequestBody User user) {
        if (StrUtil.isBlank(user.getUserName()) || StrUtil.isBlank(user.getPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        userService.updatePassword(user);
        return Result.success();
    }
}
