package com.mk.mkslservice.controller;

import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.model.Note;
import com.mk.mkslservice.service.NoteService;
import com.mk.mkslservice.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 笔记控制器
 *
 * @author Shixue
 */
@RestController
@RequestMapping("/note")
@Api(tags = "笔记接口")
public class NoteController {

    @Resource
    private NoteService noteService;

    @PostMapping("/add")
    @ApiOperation("新增笔记")
    public Result add(@RequestBody Note note) {
        noteService.add(note);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除笔记")
    public Result deleteById(@PathVariable Integer id) {
        noteService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    @ApiOperation("批量删除笔记")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        noteService.deleteBatch(ids);
        return Result.success();
    }

    @PutMapping("/update")
    @ApiOperation("更新笔记")
    public Result updateById(@RequestBody Note note) {
        noteService.updateById(note);
        return Result.success();
    }

    @GetMapping("/selectById/{id}")
    @ApiOperation("根据id查询笔记")
    public Result selectById(@PathVariable Integer id) {
        Note note = noteService.selectById(id);
        return Result.success(note);
    }

    @GetMapping("/selectAll")
    @ApiOperation("查询所有笔记")
    public Result selectAll(Note note) {
        List<Note> list = noteService.selectAll(note);
        return Result.success(list);
    }

    @GetMapping("/selectPage")
    @ApiOperation("分页查询笔记")
    public Result selectPage(Note note,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Note> page = noteService.selectPage(note, pageNum, pageSize);
        return Result.success(page);
    }

}