package com.mk.mkslservice.controller;

import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.model.Category;
import com.mk.mkslservice.service.CategoryService;
import com.mk.mkslservice.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 帖子类别控制器
 *
 * @author Shixue
 */
@RestController
@RequestMapping("/category")
@Api(tags = "帖子分类管理")
public class CategoryController {

    @Resource
    private CategoryService categoryService;

    @PostMapping("/add")
    @ApiOperation("新增帖子分类")
    public Result add(@RequestBody Category category) {
        categoryService.add(category);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除帖子分类")
    public Result deleteById(@PathVariable Integer id) {
        categoryService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    @ApiOperation("批量删除帖子分类")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        categoryService.deleteBatch(ids);
        return Result.success();
    }

    @PutMapping("/update")
    @ApiOperation("修改帖子分类")
    public Result updateById(@RequestBody Category category) {
        categoryService.updateById(category);
        return Result.success();
    }

    @GetMapping("/selectById/{id}")
    @ApiOperation("根据ID查询帖子分类")
    public Result selectById(@PathVariable Integer id) {
        Category category = categoryService.selectById(id);
        return Result.success(category);
    }

    @GetMapping("/selectAll")
    @ApiOperation("查询所有帖子分类")
    public Result selectAll(Category category) {
        List<Category> list = categoryService.selectAll(category);
        return Result.success(list);
    }

    @GetMapping("/selectPage")
    @ApiOperation("分页查询帖子分类")
    public Result selectPage(Category category,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Category> page = categoryService.selectPage(category, pageNum, pageSize);
        return Result.success(page);
    }

}