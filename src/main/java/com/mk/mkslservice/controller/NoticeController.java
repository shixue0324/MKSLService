package com.mk.mkslservice.controller;

import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.model.Notice;
import com.mk.mkslservice.service.NoticeService;
import com.mk.mkslservice.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 校园公告控制器
 *
 * @author Shixue
 */
@RestController
@RequestMapping("/notice")
@Api(tags = "公告管理")
public class NoticeController {
    @Resource
    private NoticeService noticeService;

    @PostMapping("/add")
    @ApiOperation("添加公告")
    public Result add(@RequestBody Notice notice) {
        noticeService.add(notice);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除公告")
    public Result deleteById(@PathVariable Integer id) {
        noticeService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    @ApiOperation("批量删除公告")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        noticeService.deleteBatch(ids);
        return Result.success();
    }

    @PutMapping("/update")
    @ApiOperation("更新公告")
    public Result updateById(@RequestBody Notice notice) {
        noticeService.updateById(notice);
        return Result.success();
    }

    @GetMapping("/selectById/{id}")
    @ApiOperation("根据id查询公告")
    public Result selectById(@PathVariable Integer id) {
        Notice notice = noticeService.selectById(id);
        return Result.success(notice);
    }

    @PutMapping("/changeState")
    @ApiOperation("修改公告轮播图状态")
    public Result changeState(@RequestBody Notice notice) {
        noticeService.changeState(notice);
        return Result.success();
    }

    @GetMapping("/selectAll")
    @ApiOperation("查询所有公告")
    public Result selectAll(Notice notice) {
        List<Notice> list = noticeService.selectAll(notice);
        return Result.success(list);
    }

    @GetMapping("/selectPage")
    @ApiOperation("分页查询公告")
    public Result selectPage(Notice notice,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Notice> page = noticeService.selectPage(notice, pageNum, pageSize);
        return Result.success(page);
    }

}