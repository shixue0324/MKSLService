package com.mk.mkslservice.controller;

import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.model.Reply;
import com.mk.mkslservice.service.ReplyService;
import com.mk.mkslservice.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 回复控制器
 *
 * @author Shixue
 */
@RestController
@RequestMapping("/reply")
@Api(tags = "回复管理接口")
public class ReplyController {

    @Resource
    private ReplyService replyService;

    @PostMapping("/add")
    @ApiOperation("添加回复")
    public Result add(@RequestBody Reply reply) {
        replyService.add(reply);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除回复")
    public Result deleteById(@PathVariable Integer id) {
        replyService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    @ApiOperation("批量删除回复")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        replyService.deleteBatch(ids);
        return Result.success();
    }

    @PutMapping("/update")
    @ApiOperation("更新回复")
    public Result updateById(@RequestBody Reply reply) {
        replyService.updateById(reply);
        return Result.success();
    }

    @GetMapping("/selectById/{id}")
    @ApiOperation("根据id查询回复")
    public Result selectById(@PathVariable Integer id) {
        Reply reply = replyService.selectById(id);
        return Result.success(reply);
    }

    @GetMapping("/selectAll")
    @ApiOperation("查询所有回复")
    public Result selectAll(Reply reply) {
        List<Reply> list = replyService.selectAll(reply);
        return Result.success(list);
    }

    @GetMapping("/selectPage")
    @ApiOperation("分页查询回复")
    public Result selectPage(Reply reply,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Reply> page = replyService.selectPage(reply, pageNum, pageSize);
        return Result.success(page);
    }

}