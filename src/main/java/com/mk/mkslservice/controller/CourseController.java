package com.mk.mkslservice.controller;

import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.model.Course;
import com.mk.mkslservice.service.CourseService;
import com.mk.mkslservice.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户课程表控制器
 *
 * @author Shixue
 */
@RestController
@RequestMapping("/course")
@Api(tags = "课程表管理")
public class CourseController {

    @Resource
    private CourseService courseService;

    @PostMapping("/add")
    @ApiOperation("新增课程表")
    public Result add(@RequestBody Course course) {
        courseService.add(course);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("根据ID删除课程表")
    public Result deleteById(@PathVariable Integer id) {
        courseService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    @ApiOperation("批量删除课程表")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        courseService.deleteBatch(ids);
        return Result.success();
    }

    @PutMapping("/update")
    @ApiOperation("更新课程表")
    public Result updateById(@RequestBody Course course) {
        courseService.updateById(course);
        return Result.success();
    }

    @GetMapping("/selectById/{id}")
    @ApiOperation("根据ID查询课程表")
    public Result selectById(@PathVariable Integer id) {
        Course course = courseService.selectById(id);
        return Result.success(course);
    }

    @GetMapping("/selectAll")
    @ApiOperation("查询全部课程表")
    public Result selectAll(Course course) {
        List<Course> list = courseService.selectAll(course);
        return Result.success(list);
    }

    @GetMapping("/selectPage")
    @ApiOperation("分页查询课程表")
    public Result selectPage(Course course,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Course> page = courseService.selectPage(course, pageNum, pageSize);
        return Result.success(page);
    }

}