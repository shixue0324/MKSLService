package com.mk.mkslservice.controller;

import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.model.Post;
import com.mk.mkslservice.service.PostService;
import com.mk.mkslservice.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 帖子控制器
 *
 * @author Shixue
 */
@RestController
@RequestMapping("/post")
@Api(tags = "帖子操作接口")
public class PostController {

    @Resource
    private PostService postService;

    @PostMapping("/add")
    @ApiOperation("新增帖子")
    public Result add(@RequestBody Post post) {
        postService.add(post);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除帖子")
    public Result deleteById(@PathVariable Integer id) {
        postService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    @ApiOperation("批量删除帖子")
    public Result deleteBatch(@RequestBody List<Integer> ids) {
        postService.deleteBatch(ids);
        return Result.success();
    }

    @PutMapping("/update")
    @ApiOperation("更新帖子")
    public Result updateById(@RequestBody Post post) {
        postService.updateById(post);
        return Result.success();
    }

    @GetMapping("/selectById/{id}")
    @ApiOperation("根据id查询帖子")
    public Result selectById(@PathVariable Integer id) {
        Post post = postService.selectById(id);
        return Result.success(post);
    }

    @GetMapping("/selectAll")
    @ApiOperation("查询所有帖子")
    public Result selectAll(Post post) {
        List<Post> list = postService.selectAll(post);
        return Result.success(list);
    }

    @GetMapping("/selectPage")
    @ApiOperation("分页查询帖子")
    public Result selectPage(Post post,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Post> page = postService.selectPage(post, pageNum, pageSize);
        return Result.success(page);
    }

}