package com.mk.mkslservice.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.exception.CustomException;
import com.mk.mkslservice.mapper.PostMapper;
import com.mk.mkslservice.model.Post;
import com.mk.mkslservice.utils.ResultCodeEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 帖子服务层
 *
 * @author Shixue
 */
@Service
public class PostService {
    @Resource
    private PostMapper postMapper;

    public void add(Post post) {
        Date currentTime = new Date();
        post.setTime(currentTime);
        if(ObjectUtil.isEmpty(post.getTitle())){
            throw new CustomException(ResultCodeEnum.TITLE_NULL_ERROR);
        } else if (ObjectUtil.isNull(post.getContent())) {
            throw new CustomException(ResultCodeEnum.CONTENT_NULL_ERROR);
        }
        postMapper.insert(post);
    }

    public void deleteById(Integer id) {
        postMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            postMapper.deleteById(id);
        }
    }

    public void updateById(Post post) {
        if(ObjectUtil.isEmpty(post.getTitle())){
            throw new CustomException(ResultCodeEnum.TITLE_NULL_ERROR);
        } else if (ObjectUtil.isNull(post.getContent())) {
            throw new CustomException(ResultCodeEnum.CONTENT_NULL_ERROR);
        }
        postMapper.updateById(post);
    }

    public Post selectById(Integer id) {
        return postMapper.selectById(id);
    }

    public List<Post> selectAll(Post post) {
        return postMapper.selectAll(post);
    }

    public PageInfo<Post> selectPage(Post post, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Post> list = postMapper.selectAll(post);
        return PageInfo.of(list);
    }
}