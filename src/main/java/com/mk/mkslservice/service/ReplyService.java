package com.mk.mkslservice.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.mapper.ReplyMapper;
import com.mk.mkslservice.model.Reply;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 回复服务层
 *
 * @author Shixue
 */
@Service
public class ReplyService {
    @Resource
    private ReplyMapper replyMapper;

    public void add(Reply reply) {
        Date currentTime = new Date();
        reply.setTime(currentTime);
        replyMapper.insert(reply);
    }

    public void deleteById(Integer id) {
        replyMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            replyMapper.deleteById(id);
        }
    }

    public void updateById(Reply reply) {
        replyMapper.updateById(reply);
    }

    public Reply selectById(Integer id) {
        return replyMapper.selectById(id);
    }

    public List<Reply> selectAll(Reply reply) {
        return replyMapper.selectAll(reply);
    }

    public PageInfo<Reply> selectPage(Reply reply, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Reply> list = replyMapper.selectAll(reply);
        return PageInfo.of(list);
    }
}