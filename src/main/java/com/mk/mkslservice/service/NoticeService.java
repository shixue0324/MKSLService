package com.mk.mkslservice.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.exception.CustomException;
import com.mk.mkslservice.mapper.NoticeMapper;
import com.mk.mkslservice.model.Notice;
import com.mk.mkslservice.utils.ResultCodeEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 校园公告服务层
 *
 * @author Shixue
 */
@Service
public class NoticeService {
    @Resource
    private NoticeMapper noticeMapper;

    public void add(Notice notice) {
        Date currentTime = new Date();
        notice.setTime(currentTime);
        notice.setContent(addImageStyles(notice.getContent()));
        noticeMapper.insert(notice);
    }

    public void deleteById(Integer id) {
        noticeMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            noticeMapper.deleteById(id);
        }
    }

    public void updateById(Notice notice) {
        notice.setContent(addImageStyles(notice.getContent()));
        noticeMapper.updateById(notice);
    }

    public Notice selectById(Integer id) {
        return noticeMapper.selectById(id);
    }

    public List<Notice> selectAll(Notice notice) {
        return noticeMapper.selectAll(notice);
    }

    public PageInfo<Notice> selectPage(Notice notice, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Notice> list = noticeMapper.selectAll(notice);
        return PageInfo.of(list);
    }

    public void changeState(Notice notice) {
        if (ObjectUtil.isNull(selectById(notice.getInfoId()).getImage())) {
            throw new CustomException(ResultCodeEnum.NO_IMAGE);
        }
        if (noticeMapper.countState() >= 5 && ObjectUtil.equal(1, notice.getState())) {
            throw new CustomException(ResultCodeEnum.IMAGE_LIMIT);
        }
        noticeMapper.updateById(notice);
    }

    public static String addImageStyles(String html) {
        String regex = "<img\\s+[^>]*src\\s*=\\s*\"([^\"]*)\"[^>]*>";
        String replacement = "<img style=\"max-width:100%; width:500px;\" src=\"$1\">";
        return html.replaceAll(regex, replacement);
    }
}