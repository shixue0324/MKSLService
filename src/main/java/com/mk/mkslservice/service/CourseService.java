package com.mk.mkslservice.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.exception.CustomException;
import com.mk.mkslservice.mapper.CourseMapper;
import com.mk.mkslservice.model.Course;
import com.mk.mkslservice.utils.ResultCodeEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 用户课程表服务层
 *
 * @author Shixue
 */
@Service
public class CourseService {
    @Resource
    private CourseMapper courseMapper;

    public void add(Course course) {
        if(ObjectUtil.isEmpty(course.getCourseName())){
            throw new CustomException(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        List<Course> courseList = selectAllByUserId(course.getUserId());
        courseList.sort(Comparator.comparing(Course::getStart));
        for (Course c : courseList) {
            judgeCourse(course, c);
        }
        if (course.getStart() > course.getEnd()) {
            throw new CustomException(ResultCodeEnum.COURSE_TIME_ERROR);
        }
        course.combineCourseTime();
        courseMapper.insert(course);
    }

    public void deleteById(Integer id) {
        courseMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            courseMapper.deleteById(id);
        }
    }

    public void updateById(Course course) {
        if (ObjectUtil.isEmpty(course.getCourseName())) {
            throw new CustomException(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        List<Course> courseList = selectAllByUserId(course.getUserId());
        courseList.sort(Comparator.comparing(Course::getStart));
        for (Course c : courseList) {
            if (!c.getCourseId().equals(course.getCourseId())) {
                judgeCourse(course, c);
            }
        }
        if (course.getStart() > course.getEnd()) {
            throw new CustomException(ResultCodeEnum.COURSE_TIME_ERROR);
        }
        course.combineCourseTime();
        courseMapper.updateById(course);
    }

    private void judgeCourse(Course course, Course c) {
        c.parseCourseTime();
        if (c.getDay() == course.getDay()) {
            for (Integer weekNumber : course.getWeekNumbers()) {
                if ((c.getStart() <= course.getStart() && c.getEnd() >= course.getStart() ||
                        c.getStart() <= course.getEnd() && c.getEnd() >= course.getEnd()) &&
                        c.getWeekNumbers().contains(weekNumber)) {
                    throw new CustomException(ResultCodeEnum.COURSE_TIME_CONFLICT);
                }
            }
        }
    }

    public Course selectById(Integer id) {
        Course course = courseMapper.selectById(id);
        course.parseCourseTime();
        return course;
    }

    public List<Course> selectAllByUserId(Integer userId) {
        return courseMapper.selectAllByUserId(userId);
    }

    public List<Course> selectAll(Course course) {
        List<Course> list = courseMapper.selectAll(course);
        for (Course c : list) {
            c.parseCourseTime();
        }
        return list;
    }

    public PageInfo<Course> selectPage(Course course, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Course> list = courseMapper.selectAll(course);
        return PageInfo.of(list);
    }
}