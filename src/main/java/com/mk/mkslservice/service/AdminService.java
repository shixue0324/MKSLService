package com.mk.mkslservice.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.constant.Constants;
import com.mk.mkslservice.exception.CustomException;
import com.mk.mkslservice.mapper.AdminMapper;
import com.mk.mkslservice.model.Admin;
import com.mk.mkslservice.utils.ResultCodeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 管理员服务层
 * @author Shixue
 * @date 2024/1/19
 */
@Service
public class AdminService {
    @Resource
    private AdminMapper adminMapper;

    public void add(Admin admin) {
        Admin dbAdmin = adminMapper.selectByName(admin.getAdminName());
        if (ObjectUtil.isNotNull(dbAdmin)) {
            throw new CustomException(ResultCodeEnum.USER_EXIST_ERROR);
        }
        if (ObjectUtil.isEmpty(admin.getPassword())) {
            admin.setPassword(Constants.DEFAULT_PASSWORD);
        }
        if (ObjectUtil.isEmpty(admin.getName())) {
            admin.setName(admin.getAdminName());
        }
        if(ObjectUtil.isEmpty(admin.getAvatar())){
            admin.setAvatar(Constants.DEFAULT_AVATAR);
        }
        adminMapper.insert(admin);
    }

    public void deleteById(Integer id) {
        if (adminMapper.selectById(id) == null) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        adminMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            adminMapper.deleteById(id);
        }
    }

    public void updateById(Admin admin) {
        adminMapper.updateById(admin);
    }

    public Admin selectById(Integer id) {
        return adminMapper.selectById(id);
    }

    public List<Admin> selectAll(Admin admin) {
        return adminMapper.selectAll(admin);
    }

    public PageInfo<Admin> selectPage(Admin admin, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Admin> list = adminMapper.selectAll(admin);
        return PageInfo.of(list);
    }

    public Admin login(Admin admin) {
        Admin dbAdmin = adminMapper.selectByName(admin.getAdminName());
        if (ObjectUtil.isNull(dbAdmin)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!admin.getPassword().equals(dbAdmin.getPassword())) {
            throw new CustomException(ResultCodeEnum.USER_ACCOUNT_ERROR);
        }
        return dbAdmin;
    }

    public void register(Admin admin) {
        Admin dbadmin = new Admin();
        BeanUtils.copyProperties(admin, dbadmin);
        add(admin);
    }

    public void updatePassword(Admin admin) {
        Admin dbAdmin = adminMapper.selectByName(admin.getAdminName());
        if (ObjectUtil.isNull(dbAdmin)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!admin.getPassword().equals(dbAdmin.getPassword())) {
            throw new CustomException(ResultCodeEnum.PARAM_PASSWORD_ERROR);
        }
        dbAdmin.setPassword(admin.getNewPassword());
        adminMapper.updateById(dbAdmin);
    }
}
