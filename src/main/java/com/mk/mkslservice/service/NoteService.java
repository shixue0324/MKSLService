package com.mk.mkslservice.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.exception.CustomException;
import com.mk.mkslservice.mapper.NoteMapper;
import com.mk.mkslservice.model.Note;
import com.mk.mkslservice.utils.ResultCodeEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 笔记服务层
 *
 * @author Shixue
 */
@Service
public class NoteService {
    @Resource
    private NoteMapper noteMapper;

    public void add(Note note) {
        Date currentTime = new Date();
        note.setNoteTime(currentTime);
        if(ObjectUtil.isEmpty(note.getTitle())){
            throw new CustomException(ResultCodeEnum.TITLE_NULL_ERROR);
        } else if (ObjectUtil.isNull(note.getContent())) {
            throw new CustomException(ResultCodeEnum.CONTENT_NULL_ERROR);
        }
        noteMapper.insert(note);
    }

    public void deleteById(Integer id) {
        noteMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            noteMapper.deleteById(id);
        }
    }

    public void updateById(Note note) {
        if(ObjectUtil.isEmpty(note.getTitle())){
            throw new CustomException(ResultCodeEnum.TITLE_NULL_ERROR);
        } else if (ObjectUtil.isNull(note.getContent())) {
            throw new CustomException(ResultCodeEnum.CONTENT_NULL_ERROR);
        }
        noteMapper.updateById(note);
    }

    public Note selectById(Integer id) {
        return noteMapper.selectById(id);
    }

    public List<Note> selectAll(Note note) {
        return noteMapper.selectAll(note);
    }

    public PageInfo<Note> selectPage(Note note, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Note> list = noteMapper.selectAll(note);
        return PageInfo.of(list);
    }
}