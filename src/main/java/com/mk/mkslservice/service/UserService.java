package com.mk.mkslservice.service;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mk.mkslservice.constant.Constants;
import com.mk.mkslservice.exception.CustomException;
import com.mk.mkslservice.mapper.UserMapper;
import com.mk.mkslservice.model.User;
import com.mk.mkslservice.utils.ResultCodeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户服务层
 *
 * @author Shixue
 * @date 2024/1/19
 */
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;

    public void add(User user) {
        User dbUser = userMapper.selectByName(user.getUserName());
        if (ObjectUtil.isNotNull(dbUser)) {
            throw new CustomException(ResultCodeEnum.USER_EXIST_ERROR);
        }
        if (ObjectUtil.isEmpty(user.getPassword())) {
            user.setPassword(Constants.DEFAULT_PASSWORD);
        }
        if (ObjectUtil.isEmpty(user.getName())) {
            user.setName(user.getUserName());
        }
        if(ObjectUtil.isEmpty(user.getAvatar())){
            user.setAvatar(Constants.DEFAULT_AVATAR);
        }
        userMapper.insert(user);
    }

    public void deleteById(Integer id) {
        userMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            userMapper.deleteById(id);
        }
    }

    public void updateById(User user) {
        userMapper.updateById(user);
    }

    public User selectById(Integer id) {
        return userMapper.selectById(id);
    }

    public List<User> selectAll(User user) {
        return userMapper.selectAll(user);
    }

    public PageInfo<User> selectPage(User user, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> list = userMapper.selectAll(user);
        return PageInfo.of(list);
    }

    public User login(User user) {
        User dbUser = userMapper.selectByName(user.getUserName());
        if (ObjectUtil.isNull(dbUser)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!user.getPassword().equals(dbUser.getPassword())) {
            throw new CustomException(ResultCodeEnum.USER_ACCOUNT_ERROR);
        }
        return dbUser;
    }

    public void register(User user) {
        User dbuser = new User();
        BeanUtils.copyProperties(user, dbuser);
        add(user);
    }

    public void updatePassword(User user) {
        User dbUser = userMapper.selectByName(user.getUserName());
        if (ObjectUtil.isNull(dbUser)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!user.getPassword().equals(dbUser.getPassword())) {
            throw new CustomException(ResultCodeEnum.PARAM_PASSWORD_ERROR);
        }
        dbUser.setPassword(user.getNewPassword());
        userMapper.updateById(dbUser);
    }
}
