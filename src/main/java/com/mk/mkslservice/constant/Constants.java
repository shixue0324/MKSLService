package com.mk.mkslservice.constant;

/**
 * @author Shixue
 * @date 2024/1/21
 */
public interface Constants {
    String DEFAULT_PASSWORD = "123456";
    String DEFAULT_AVATAR = "http://localhost:9090/files/avatar.jpg";
}
