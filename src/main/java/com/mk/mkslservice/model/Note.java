package com.mk.mkslservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 笔记
 *
 * @author Shixue
 */
@Data
@ApiModel(value = "笔记实体类", description = "笔记")
public class Note implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("笔记id")
    private Integer noteId;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("内容")
    private String content;
    @ApiModelProperty("时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date noteTime;
}