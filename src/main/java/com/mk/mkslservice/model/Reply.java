package com.mk.mkslservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 回复
 *
 * @author Shixue
 */
@Data
@ApiModel(value = "回复实体类", description = "回复")
public class Reply implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("回复ID")
    private Integer replyId;
    @ApiModelProperty("用户ID")
    private Integer userId;
    @ApiModelProperty("帖子ID")
    private Integer postId;
    @ApiModelProperty("回复内容")
    private String replyContent;
    @ApiModelProperty("回复时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;
    @ApiModelProperty("用户名")
    private String name;
    @ApiModelProperty("用户头像")
    private String avatar;
}