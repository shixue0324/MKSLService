package com.mk.mkslservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户课程表
 *
 * @author Shixue
 */
@Data
@ApiModel(value = "课程表实体类", description = "课程表")
public class Course implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("课程表id")
    private Integer courseId;
    @ApiModelProperty("课程名称")
    private String courseName;
    @ApiModelProperty("教师")
    private String teacher;
    @ApiModelProperty("上课地点")
    private String classroom;
    @ApiModelProperty(name = "上课时间", example = "上课时间(上课周次列表:星期:开始节次:结束节次)")
    private String courseTime;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty("上课周次列表")
    private List<Integer> weekNumbers;
    @ApiModelProperty("星期")
    private int day;
    @ApiModelProperty("开始节次")
    private int start;
    @ApiModelProperty("结束节次")
    private int end;

    /**
     * 解析上课时间
     */
    public void parseCourseTime() {
        String[] parts = courseTime.split(":");
        weekNumbers = new ArrayList<>();
        String[] weekParts = parts[0].split(",");
        for (String weekPart : weekParts) {
            weekNumbers.add(Integer.parseInt(weekPart));
        }
        day = Integer.parseInt(parts[1]);
        start = Integer.parseInt(parts[2]);
        end = Integer.parseInt(parts[3]);
    }

    /**
     * 合成上课时间
     */
    public void combineCourseTime() {
        String weeks = weekNumbers.stream().map(Object::toString).collect(Collectors.joining(","));
        courseTime = weeks + ":" + day + ":" + start + ":" + end;
    }

}