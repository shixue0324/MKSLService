package com.mk.mkslservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 校园公告
 *
 * @author Shixue
 */
@Data
@ApiModel(value = "公告实体类", description = "公告表")
public class Notice implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("公告id")
    private Integer infoId;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("内容")
    private String content;
    @ApiModelProperty("发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;
    @ApiModelProperty("标题图")
    private String image;
    @ApiModelProperty("作者")
    private String name;
    @ApiModelProperty("轮播图状态")
    private Integer state;
}