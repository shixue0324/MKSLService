package com.mk.mkslservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 帖子类别
 *
 * @author Shixue
 */
@Data
@ApiModel(value = "帖子类别实体类", description = "帖子类别")
public class Category implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("类别id")
    private Integer typeId;
    @ApiModelProperty("类别名称")
    private String typeName;


}