package com.mk.mkslservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 管理员
 *
 * @author Shixue
 * @since 2024-01-19
 */
@Data
@ApiModel(value = "管理员实体类", description = "管理员")
public class Admin implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("管理员id")
    private Integer id;
    @ApiModelProperty("管理员用户名")
    private String adminName;
    @ApiModelProperty("管理员密码")
    private String password;
    @ApiModelProperty("管理员姓名")
    private String name;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("新密码")
    private String newPassword;
}
