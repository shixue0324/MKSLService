package com.mk.mkslservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户
 *
 * @author Shixue
 * @since 2024-01-19
 */
@Data
@ApiModel(value = "用户实体类", description = "用户")
public class User implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty("用户账号")
    private String userName;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("用户名")
    private String name;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("电话号码")
    private String phone;
    @ApiModelProperty("头像URL")
    private String avatar;
    @ApiModelProperty("个人简介")
    private String introduction;
    @ApiModelProperty("性别")
    private String sex;
    @ApiModelProperty("新密码")
    private String newPassword;
}
