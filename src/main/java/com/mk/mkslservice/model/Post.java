package com.mk.mkslservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 帖子
 *
 * @author Shixue
 */
@Data
@ApiModel(value = "帖子实体类", description = "帖子表")
public class Post implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("帖子id")
    private Integer postId;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty("帖子标题")
    private String title;
    @ApiModelProperty("帖子内容")
    private String content;
    @ApiModelProperty("帖子类型id")
    private Integer typeId;
    @ApiModelProperty("帖子创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;
    @ApiModelProperty("作者名")
    private String name;
    @ApiModelProperty("帖子类型名")
    private String typeName;
}