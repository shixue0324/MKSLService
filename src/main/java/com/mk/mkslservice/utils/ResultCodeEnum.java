package com.mk.mkslservice.utils;

/**
 * @author Shixue
 */

public enum ResultCodeEnum {
    SUCCESS("200", "成功"),
    SUCCESS_LOGIN("200", "登录成功"),
    PARAM_ERROR("400", "参数异常"),
    PARAM_LOST_ERROR("4001", "参数缺失"),
    NO_IMAGE("4002", "没有标题图,不可设置轮播图"),
    IMAGE_LIMIT("4003", "最多设置五张轮播图"),

    SYSTEM_ERROR("500", "系统异常"),
    USER_EXIST_ERROR("5001", "用户名已存在"),
    USER_NOT_LOGIN("5002", "用户未登录"),
    USER_ACCOUNT_ERROR("5003", "账号或密码错误"),
    USER_NOT_EXIST_ERROR("5004", "用户不存在"),
    PARAM_PASSWORD_ERROR("5005", "原密码输入错误"),
    COURSE_TIME_CONFLICT("5006", "课程时间冲突"),
    COURSE_TIME_ERROR("5007", "开始节次必须小于结束节次"),
    TITLE_NULL_ERROR("5008","标题不能为空"),
    CONTENT_NULL_ERROR("5009","内容不能为空")
    ;

    public String code;
    public String msg;

    ResultCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
