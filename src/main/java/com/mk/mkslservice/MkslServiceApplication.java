package com.mk.mkslservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Shixue
 */
@SpringBootApplication
@MapperScan("com.mk.mkslservice.mapper") // 扫描Mapper接口)
public class MkslServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MkslServiceApplication.class, args);
    }

}
