package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Note;

import java.util.List;

/**
 * 笔记管理接口
 *
 * @author Shixue
 */
public interface NoteMapper {
    int insert(Note note);

    int deleteById(Integer id);

    int updateById(Note note);

    Note selectById(Integer id);

    List<Note> selectAll(Note note);
}