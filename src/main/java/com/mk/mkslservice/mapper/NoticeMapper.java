package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Notice;

import java.util.List;

/**
 * 校园公告接口
 *
 * @author Shixue
 */
public interface NoticeMapper {
    int insert(Notice notice);

    int deleteById(Integer id);

    int updateById(Notice notice);

    Notice selectById(Integer id);

    List<Notice> selectAll(Notice notice);

    int countState();
}