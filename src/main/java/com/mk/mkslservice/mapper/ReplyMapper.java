package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Reply;

import java.util.List;

/**
 * 回复管理接口
 *
 * @author Shixue
 */
public interface ReplyMapper {
    int insert(Reply reply);

    int deleteById(Integer id);

    int updateById(Reply reply);

    Reply selectById(Integer id);

    List<Reply> selectAll(Reply reply);
}