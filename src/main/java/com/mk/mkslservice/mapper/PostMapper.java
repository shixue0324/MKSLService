package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Post;

import java.util.List;

/**
 * 帖子管理接口
 *
 * @author Shixue
 */
public interface PostMapper {
    int insert(Post post);

    int deleteById(Integer id);

    int updateById(Post post);

    Post selectById(Integer id);

    List<Post> selectAll(Post post);
}