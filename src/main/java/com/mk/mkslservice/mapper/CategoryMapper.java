package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Category;

import java.util.List;

/**
 * 分类管理接口
 *
 * @author Shixue
 */
public interface CategoryMapper {

    int insert(Category category);

    int deleteById(Integer id);

    int updateById(Category category);

    Category selectById(Integer id);

    List<Category> selectAll(Category category);
}