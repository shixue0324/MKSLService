package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.User;

import java.util.List;

/**
 * 用户管理接口
 *
 * @author Shixue
 */
public interface UserMapper {
    int insert(User user);

    int deleteById(Integer id);

    int updateById(User user);

    User selectById(Integer id);

    List<User> selectAll(User user);

    User selectByName(String name);
}