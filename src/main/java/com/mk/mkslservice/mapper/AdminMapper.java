package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Admin;

import java.util.List;

/**
 * 管理员接口
 *
 * @author Shixue
 */
public interface AdminMapper {
    int insert(Admin admin);

    int deleteById(Integer id);

    int updateById(Admin admin);

    Admin selectById(Integer id);

    List<Admin> selectAll(Admin admin);

    Admin selectByName(String adminName);
}