package com.mk.mkslservice.mapper;

import com.mk.mkslservice.model.Course;

import java.util.List;

/**
 * 课程表管理接口
 *
 * @author Shixue
 */
public interface CourseMapper {
    int insert(Course course);

    int deleteById(Integer id);

    int updateById(Course course);

    Course selectById(Integer id);

    List<Course> selectAll(Course course);

    List<Course> selectAllByUserId(Integer userId);
}