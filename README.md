# 毕设闽科校园生活后端

#### 介绍
鄙人的渣渣毕业设计闽科校园生活，仅供代码参考，且勿照搬
1.  APP：https://gitee.com/shixue0324/MKSLAPP
2.  服务器：https://gitee.com/shixue0324/MKSLService
3.  前端：https://gitee.com/shixue0324/MKSLWeb

#### 软件架构
原生安卓APP，服务器Spring Boot，前端后台管理Vue.js

#### 安装教程
1.  Android studio
2.  IntelliJ Idea
3.  Node.js
4.  VS Code

#### 使用说明
1.  MKSLAPP用Android studio运行，需在MKSLAPP\app\src\main\java\com\mk\mkslapp\utils下将RetrofitUtil.java文件里的BASE_URL修改成本地服务器的ip地址，否则模拟器识别不到接口。
2.  MKSLService用IntelliJ Idea运行，运行前需在MKSLService\src\main\resources下修改application.yml文件内的数据库配置，默认连接MySQL数据库端口3306，账号root，密码123456，数据库名称mksl,默认端口9090。
3.  MKSLWeb用VS Code或在文件下用命令行npm install下载运行库，输入npm run serve运行，需下载node.js，默认端口8080。
4.  mksl.sql为数据库脚本文件，运行前请先创建名为mksl的数据库后再在该数据库运行脚本。

#### 页面效果
![](files/image.png)![](files/image1.png)![](files/image2.png)![](files/image3.png)![](files/image4.png)![](files/image5.png)![](files/image6.png)![](files/image7.png)

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技
1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
